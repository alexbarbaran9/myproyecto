from Vehiculo import Vehiculo
class Moto(Vehiculo):
    def __init__(self,pColor,pRuedas,pCilindrada):
        super().__init__(pColor, pRuedas)
        self.__cilindrada = pCilindrada
    def getCilindrada(self):
        return self.__cilindrada
    def __str__(self):
        return str(super().__str__()) + ". Es una moto, y su cilindrada es de " + str(self.__cilindrada) 
    
print('joan commit')