class Vehiculo:
    def __init__(self,pColor,pRuedas):
        self.__color = pColor
        self.__ruedas = pRuedas
    def __str__(self):
        return "El vehiculo es de color " + self.__color + " y tiene " + str(self.__ruedas) + " ruedas"
    def getColor(self):
        return self.__color
    def getRuedas(self):
        return self.__ruedas
    def setColor(self,color):
        self.__color = color
    def setRuedas(self,ruedas):
        self.__ruedas = ruedas

""" color = input( "Dime un color")
ruedas = input ("Dime el numero de ruedas")
vehiculo1 = Vehiculo(color, ruedas)
print(vehiculo1)
vehiculo1.setRuedas(20)
print(vehiculo1) """

