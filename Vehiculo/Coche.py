from Vehiculo import Vehiculo
class Coche(Vehiculo):
    def __init__(self, pColor, pRuedas,pVelocidad,pNumeroPuertas):
        super().__init__(pColor, pRuedas)
        self.__velocidad = pVelocidad
        self.__puertas = pNumeroPuertas
    def getVelocidad(self):
        return self.__velocidad
    def getPuertas(self):
        return self.__puertas
    def __str__(self):
        return str(super().__str__()) + ". Es un coche, y su velocidad es de " + str(self.__velocidad) + " km max, y tiene " + str(self.__puertas) + " puertas"


